using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Services;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            
            services.AddSingleton<IWithdrawService, WithdrawService>();
            
            services.AddSingleton<ILogWriteService, LogWriteService>();
            
            services.AddSingleton<ILogService, LogService>((_) => new LogService(".\\..\\Transactions.log"));

            services.AddSingleton<IParkingService, ParkingService>();
            
            services.AddSingleton<ITransactionsService, TransactionsService>(provider => new TransactionsService(
                provider.GetService<ILogService>(),
                provider.GetService<ILogWriteService>().LogWriteTimerService
            ));
            
            services.AddSingleton<IVehiclesService, VehiclesService>((provider) => new VehiclesService(
                provider.GetService<IWithdrawService>().WithdrawTimerService,
                provider.GetService<ITransactionsService>()
            ));
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
