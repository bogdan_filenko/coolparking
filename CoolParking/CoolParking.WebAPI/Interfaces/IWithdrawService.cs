using CoolParking.BL.Interfaces;

namespace CoolParking.WebAPI.Interfaces
{
    public interface IWithdrawService
    {
        ITimerService WithdrawTimerService { get; }
    }
}