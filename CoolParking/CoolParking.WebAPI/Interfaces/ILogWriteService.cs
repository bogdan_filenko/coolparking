using CoolParking.BL.Interfaces;

namespace CoolParking.WebAPI.Interfaces
{
    public interface ILogWriteService
    {
        ITimerService LogWriteTimerService { get; }
    }
}