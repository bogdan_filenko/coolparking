using CoolParking.BL.Services;
using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.Interfaces;
using static CoolParking.BL.Models.Settings;

namespace CoolParking.WebAPI.Services
{
    public class LogWriteService : ILogWriteService
    {
        public ITimerService LogWriteTimerService { get; }
        public LogWriteService()
        {
            LogWriteTimerService = new TimerService(LogWritePeriod);
        }
    }
}