using CoolParking.BL.Services;
using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.Interfaces;
using static CoolParking.BL.Models.Settings;

namespace CoolParking.WebAPI.Services
{
    public class WithdrawService : IWithdrawService
    {
        public ITimerService WithdrawTimerService { get; }
        public WithdrawService()
        {
            WithdrawTimerService = new TimerService(WriteOffPeriod);
        }
    }
}