using CoolParking.BL.Models;
using System.Text.Json;
using System;
using System.Linq;

namespace CoolParking.WebAPI.Services
{
    public static class JsonBodyValidator
    {
        public static string ValidateVehicleId(JsonElement bodyModel)
        {
            if (bodyModel.TryGetProperty("id", out JsonElement vehicleIdModel) && !string.IsNullOrEmpty(vehicleIdModel.ToString()))
            {
                return vehicleIdModel.ToString();
            }
            return null;
        }
        public static decimal? ValidateBalance(JsonElement bodyModel)
        {
            if (!bodyModel.TryGetProperty("balance", out JsonElement balanceModel) || !balanceModel.TryGetDecimal(out decimal balance))
            {
                return null;
            }
            if (balance >= 0)
            {
                return balance;
            }
            return null;
        }
        public static decimal? ValidateSum(JsonElement bodyModel)
        {
            if (bodyModel.TryGetProperty("Sum", out JsonElement sumModel) && sumModel.TryGetDecimal(out decimal sum))
            {
                return sum;
            }
            return null;
        }
        public static VehicleType? ValidateVehicleType(JsonElement bodyModel)
        {
            if (!bodyModel.TryGetProperty("vehicleType", out JsonElement vehicleTypeModel) || !vehicleTypeModel.TryGetByte(out byte vehicleTypeNumber))
            {
                return null;
            }
            if (Enum.IsDefined(typeof(VehicleType), vehicleTypeNumber))
            {
                return (VehicleType)vehicleTypeNumber;
            }
            return null;
        }
        public static Vehicle ValidateVehicleBody(string jsonBody)
        {
            JsonElement bodyModel = JsonSerializer.Deserialize<JsonElement>(jsonBody);
            if (bodyModel.EnumerateObject().Count() != 3)
            {
                throw new ArgumentException("Incorrect number of body properties");
            }
            string vehicleId = ValidateVehicleId(bodyModel);
            if (vehicleId == null)
            {
                throw new ArgumentException("Vehicle id property is not found or empty");
            }
            decimal? balance = ValidateBalance(bodyModel);
            if (balance == null)
            {
                throw new ArgumentException("Vehicle balance property is not found or empty, either negative");
            }
            VehicleType? vehicleType = ValidateVehicleType(bodyModel);
            if (vehicleType == null)
            {
                throw new ArgumentException("Vehicle type property is not found or not in correct format");
            }
            return new Vehicle(vehicleId, (VehicleType)vehicleType, (decimal)balance);
        }
    }
}