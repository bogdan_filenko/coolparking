using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]/")]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        public ParkingController(IParkingService parking)
        {
            _parkingService = parking;
        }
        [HttpGet]
        [Route("balance")]
        public IActionResult Balance()
        {
            return Ok(_parkingService.GetBalance());
        }
        [HttpGet]
        [Route("capacity")]
        public IActionResult Capacity()
        {
            return Ok(_parkingService.GetCapacity());
        }
        [HttpGet]
        [Route("freePlaces")]
        public IActionResult FreePlaces()
        {
            return Ok(_parkingService.GetFreePlaces());
        }
    }
}