using Microsoft.AspNetCore.Mvc;
using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Services;
using System.IO;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]/")]
    public class VehiclesController : ControllerBase
    {
        private readonly IVehiclesService _vehiclesService;
        public VehiclesController(IVehiclesService vehiclesService)
        {
            _vehiclesService = vehiclesService;
        }
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_vehiclesService.GetVehiclesList());
        }
        [HttpGet("{vehicleId}")]
        public IActionResult Get(string vehicleId)
        {
            try
            {
                return Ok(_vehiclesService.GetVehicle(vehicleId));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return NotFound(ex.Message);
            }
        }
        [HttpPost]
        public IActionResult Post()
        {
            try
            {
                string jsonBody = string.Empty;
                using (var reader = new StreamReader(Request.Body))
                {
                    jsonBody = reader.ReadToEndAsync().Result;
                }
                if (string.IsNullOrEmpty(jsonBody))
                {
                    throw new ArgumentException("Body is empty");
                }
                Vehicle vehicle = JsonBodyValidator.ValidateVehicleBody(jsonBody);
                _vehiclesService.AddVehicle(vehicle);
                return Created(new Uri($"http://localhost:5000/api/vehicles/{vehicle.Id}"), vehicle);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete("{vehicleId}")]
        public IActionResult Delete(string vehicleId)
        {
            try
            {
                _vehiclesService.RemoveVehicle(vehicleId);
                return NoContent();
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}