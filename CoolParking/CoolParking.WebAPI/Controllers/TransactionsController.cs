using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using System;
using System.IO;
using System.Linq;
using System.Text.Json;
using CoolParking.WebAPI.Services;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        private readonly ITransactionsService _transactionsService;
        public TransactionsController(ITransactionsService transactionsService)
        {
            _transactionsService = transactionsService;
        }
        [HttpGet]
        [Route("last")]
        public IActionResult LastTransactions()
        {
            return Ok(_transactionsService.GetLastTransactions());
        }
        [HttpGet]
        [Route("all")]
        public IActionResult AllTransactions()
        {
            try
            {
                return Ok(_transactionsService.GetAllTransactionsHistory());
            }
            catch (InvalidOperationException ex)
            {
                return NotFound(ex.Message);
            }
        }
        [HttpPut]
        [Route("topUpVehicle")]
        public IActionResult TopUpVehicle()
        {
            try
            {
                string jsonBody = string.Empty;
                using (var reader = new StreamReader(Request.Body))
                {
                    jsonBody = reader.ReadToEndAsync().Result;
                }
                if (string.IsNullOrEmpty(jsonBody))
                {
                    throw new ArgumentException("Request body is empty");
                }
                JsonElement bodyModel = JsonSerializer.Deserialize<JsonElement>(jsonBody);
                if (bodyModel.EnumerateObject().Count() != 2)
                {
                    throw new ArgumentException("Incorrect number of body properties");
                }
                string vehicleId = JsonBodyValidator.ValidateVehicleId(bodyModel);
                if (vehicleId == null)
                {
                    throw new ArgumentException("Vehicle id property is not found or empty");   
                }
                decimal? sum = JsonBodyValidator.ValidateSum(bodyModel);
                if (sum == null)
                {
                    throw new ArgumentException("Sum property is not found or empty"); 
                }
                return Ok(_transactionsService.TopUpVehicle(vehicleId, (decimal)sum));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}