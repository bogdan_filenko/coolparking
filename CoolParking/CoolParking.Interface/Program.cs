﻿using System;
using CoolParking.Interface.Interfaces;
using CoolParking.Interface.Modules;

namespace CoolParking.Interface
{
    class Program
    {
        static void Main(string[] args)
        {
            IMenu menu = new Menu();
            ((Menu)menu).Start();
        }
    }
}
