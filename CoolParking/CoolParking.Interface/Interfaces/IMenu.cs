using CoolParking.BL.Models;

namespace CoolParking.Interface.Interfaces
{
    public interface IMenu
    {
        void ShowMainMenu();
        void ShowCurrentParkingBalance();
        void ShowCurrentProfit();
        void ShowFreePlaces();
        void ShowTransactions();
        void ShowAllTransactionsHistory();
        void ShowParkedVehicles();
        void ShowNewVehicleMenu();
        void ShowRemovingMenu();
        void ShowToppingUpMenu();
        void NotifyVehicleAdded(Vehicle vehicle);
        void NotifyVehicleRemoved(Vehicle vehicle);
        void NotifyVehicleBalanceToppedUp(Vehicle vehicle);
    }
}