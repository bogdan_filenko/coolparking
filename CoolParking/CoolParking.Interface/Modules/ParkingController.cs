using CoolParking.Interface.Interfaces;
using System.Text.Json;
using System.Linq;
using System.Net.Http;
using System;
using System.Text;
using CoolParking.BL.Models;

namespace CoolParking.Interface.Modules
{
    public class ParkingController : IParkingController
    {
        private readonly HttpClient _client;
        private readonly string _baseUrlPath;
        public event Action<Vehicle> Notify;
        public ParkingController()
        {
            _baseUrlPath = "http://localhost:5000/api/";
            _client = new HttpClient();
        } 
        public decimal GetCurrentParkingBalance()
        {
            HttpResponseMessage response = _client.Send(new HttpRequestMessage(HttpMethod.Get, _baseUrlPath + "parking/balance"));
            return decimal.Parse(response.Content.ReadAsStringAsync().Result);
        }
        public decimal GetCurrentProfit()
        {
            HttpResponseMessage response = _client.Send(new HttpRequestMessage(HttpMethod.Get, _baseUrlPath + "transactions/last"));
            TransactionInfo[] lastTransactions = JsonSerializer.Deserialize<TransactionInfo[]>(response.Content.ReadAsStringAsync().Result);
            return lastTransactions.Aggregate(0.0m, (x, y) => x + y.Sum);
        }
        public (int, int) GetFreePlacesWithTotalCapacity()
        {
            HttpResponseMessage response = _client.Send(new HttpRequestMessage(HttpMethod.Get, _baseUrlPath + "parking/freePlaces"));
            int freePlacesNumber = int.Parse(response.Content.ReadAsStringAsync().Result);
            response = _client.Send(new HttpRequestMessage(HttpMethod.Get, _baseUrlPath + "parking/capacity"));
            int capacity = int.Parse(response.Content.ReadAsStringAsync().Result);
            return (freePlacesNumber, capacity);
        }
        public string GetTransactions()
        {
            StringBuilder transactionsInfoBuilder = new StringBuilder("Current period`s transactions:\n");
            HttpResponseMessage response = _client.Send(new HttpRequestMessage(HttpMethod.Get, _baseUrlPath + "transactions/last"));
            foreach (var transaction in JsonSerializer.Deserialize<TransactionInfo[]>(response.Content.ReadAsStringAsync().Result))
            {
                transactionsInfoBuilder.Append($"\t{transaction.ToString()}\n");
            }
            return transactionsInfoBuilder.ToString();
        }
        public string GetAllTransactionsHistory()
        {
            HttpResponseMessage response = _client.Send(new HttpRequestMessage(HttpMethod.Get, _baseUrlPath + "transactions/all"));
            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsStringAsync().Result;
            }
            else
            {
                throw new InvalidOperationException(response.Content.ReadAsStringAsync().Result);
            }
        }
        public string GetParkedVehicles()
        {
            StringBuilder vehiclesListBuilder = new StringBuilder("Parked vehicles:\n");
            HttpResponseMessage response = _client.Send(new HttpRequestMessage(HttpMethod.Get, _baseUrlPath + "vehicles"));
            Vehicle[] vehicles = JsonSerializer.Deserialize<Vehicle[]>(response.Content.ReadAsStringAsync().Result);
            foreach (var vehicle in vehicles)
            {
                vehiclesListBuilder.Append('\t' + vehicle.ToString() + '\n');
            }
            return vehiclesListBuilder.ToString();
        }
        public void AddVehicle(VehicleType vehicleType, decimal balance)
        {
            HttpResponseMessage response = _client.Send(new HttpRequestMessage(HttpMethod.Get, _baseUrlPath + "vehicles"));
            Vehicle[] vehicles = JsonSerializer.Deserialize<Vehicle[]>(response.Content.ReadAsStringAsync().Result);
            string vehicleId = Vehicle.GenerateRandomRegistrationPlateNumber();
            while (vehicles.FirstOrDefault(v => v.Id == vehicleId) != default)
            {
            vehicleId = Vehicle.GenerateRandomRegistrationPlateNumber(); 
            }
            Vehicle vehicle = new Vehicle(vehicleId, vehicleType, balance);
            var content = new StringContent(JsonSerializer.Serialize<Vehicle>(vehicle), Encoding.UTF8, "application/json");
            response = _client.PostAsync(_baseUrlPath + "vehicles", content).Result;
            if (response.IsSuccessStatusCode)
            {
                Notify?.Invoke(vehicle);  
            }
            else
            {
                Notify = null;
                throw new InvalidOperationException(response.Content.ReadAsStringAsync().Result);
            }
        }
        public void RemoveVehicle(string vehicleId)
        {
            HttpResponseMessage response = _client.Send(new HttpRequestMessage(HttpMethod.Get, _baseUrlPath + $"vehicles/{vehicleId}"));
            if (!response.IsSuccessStatusCode)
            {
                Notify = null;
                throw new InvalidOperationException(response.Content.ReadAsStringAsync().Result);   
            }
            Console.WriteLine("Test");
            Vehicle removingVehicle = JsonSerializer.Deserialize<Vehicle>(response.Content.ReadAsStringAsync().Result);
            response = _client.Send(new HttpRequestMessage(HttpMethod.Delete, _baseUrlPath + $"vehicles/{vehicleId}"));
            if (response.IsSuccessStatusCode)
            {
                Notify?.Invoke(removingVehicle);
            }
            else
            {
                Notify = null;
                throw new InvalidOperationException(response.Content.ReadAsStringAsync().Result);  
            }
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var content = new StringContent(JsonSerializer.Serialize(new
            {
                id = vehicleId,
                Sum = sum
            }), Encoding.UTF8, "application/json");
            HttpResponseMessage response = _client.PutAsync(_baseUrlPath + "transactions/topUpVehicle", content).Result;
            if (response.IsSuccessStatusCode)
            {
                Notify?.Invoke(JsonSerializer.Deserialize<Vehicle>(response.Content.ReadAsStringAsync().Result));
            }
            else
            {
                Notify = null;
                throw new InvalidOperationException(response.Content.ReadAsStringAsync().Result);
            }
        }
    }
}