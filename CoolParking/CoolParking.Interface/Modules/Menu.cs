using CoolParking.Interface.Interfaces;
using System;
using CoolParking.BL.Models;

namespace CoolParking.Interface.Modules
{
    public class Menu : IMenu
    {
        private readonly ParkingController _parkingController;
        private bool _isApplicationActive;
        public Menu()
        {
            _isApplicationActive = true;
            _parkingController = new ParkingController();
        }
        public void Start()
        {
            while (_isApplicationActive)
            {
                try
                {
                    ShowMainMenu();
                    _ChooseAction(Console.ReadKey(true).KeyChar);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey(true);
                }
            }
        }
        private void _ChooseAction(char key)
        {
            switch(key)
            {
                case '0':
                    ShowCurrentParkingBalance();
                    break;
                case '1':
                    ShowCurrentProfit();
                    break;
                case '2':
                    ShowFreePlaces();
                    break;
                case '3':
                    ShowTransactions();
                    break;
                case '4':
                    ShowAllTransactionsHistory();
                    break;
                case '5':
                    ShowParkedVehicles();
                    break;
                case '6':
                    _parkingController.Notify += NotifyVehicleAdded;
                    ShowNewVehicleMenu();
                    break;
                case '7':
                    _parkingController.Notify += NotifyVehicleRemoved;
                    ShowRemovingMenu();
                    break;
                case '8':
                    _parkingController.Notify += NotifyVehicleBalanceToppedUp;
                    ShowToppingUpMenu();
                    break;
                case '9':
                    _isApplicationActive = false;
                    break;
                default:
                    throw new Exception("Unknown option");
            }
        }
        public void ShowMainMenu()
        {
            Console.Clear();
            Console.WriteLine("\n=============== Main menu ===============\n");
            Console.WriteLine("\t0. Show currect parking balance");
            Console.WriteLine("\t1. Show current profit");
            Console.WriteLine("\t2. Show free places");
            Console.WriteLine("\t3. Show last transactions");
            Console.WriteLine("\t4. Show all transactions history");
            Console.WriteLine("\t5. Show parked vehicles");
            Console.WriteLine("\t6. Add vehicle");
            Console.WriteLine("\t7. Remove vehicle");
            Console.WriteLine("\t8. Top up vehicle");
            Console.WriteLine("\t9. Exit");
        }
        public void ShowCurrentParkingBalance()
        {
            Console.Clear();
            Console.WriteLine($"Current parking balance: {_parkingController.GetCurrentParkingBalance()}$");
        }
        public void ShowCurrentProfit()
        {
            Console.Clear();
            Console.WriteLine($"Current parking profit: {_parkingController.GetCurrentProfit()}$");
        }
        public void ShowFreePlaces()
        {
            Console.Clear();
            (int, int) freePlacesWithTotalCapacity = _parkingController.GetFreePlacesWithTotalCapacity();
            Console.WriteLine($"Places statistic: {freePlacesWithTotalCapacity.Item1}/{freePlacesWithTotalCapacity.Item2} free");
        }
        public void ShowTransactions()
        {
            Console.Clear();
            Console.WriteLine(_parkingController.GetTransactions());
        }
        public void ShowAllTransactionsHistory()
        {
            Console.Clear();
            Console.WriteLine(_parkingController.GetAllTransactionsHistory());
        }
        public void ShowParkedVehicles()
        {
            Console.Clear();
            Console.WriteLine(_parkingController.GetParkedVehicles());
        }
        public void ShowNewVehicleMenu()
        {
            Console.Clear();
            Console.Write("Type vehicle type (Passenger car/Truck/Bus/Motorcycle): ");
            VehicleType vehicleType = default;
            switch (Console.ReadLine())
            {
                case "Passenger car":
                    vehicleType = VehicleType.PassengerCar;
                    break;
                case "Bus":
                    vehicleType = VehicleType.Bus;
                    break;
                case "Truck":
                    vehicleType = VehicleType.Truck;
                    break;
                case "Motorcycle":
                    vehicleType = VehicleType.Motorcycle;
                    break;
                default:
                    throw new ArgumentException("Unknown vehicle type");
            }
            Console.Write("Type vehicle balance (more than 0): ");
            decimal balance = Convert.ToDecimal(Console.ReadLine());
            _parkingController.AddVehicle(vehicleType, balance);
        }
        public void ShowRemovingMenu()
        {
            Console.Write("Type vehicle id in format XX-YYYY-XX (X - [A-Z] | Y - [0-9]): ");
            string vehicleId = Console.ReadLine();
            if (string.IsNullOrEmpty(vehicleId))
            {
                throw new ArgumentException("Vehicle id cannot be empty");
            }
            _parkingController.RemoveVehicle(vehicleId);
        }
        public void ShowToppingUpMenu()
        {
            Console.Write("Type vehicle id in format XX-YYYY-XX (X - [A-Z] | Y - [0-9]): ");
            string vehicleId = Console.ReadLine();
            Console.Write("Type topping up sum (more than 0): ");
            decimal sum = Convert.ToDecimal(Console.ReadLine());
            _parkingController.TopUpVehicle(vehicleId, sum);
        }
        public void NotifyVehicleAdded(Vehicle vehicle)
        {
            Console.Clear();
            Console.WriteLine($"{vehicle.ToString()} have been added");
            _parkingController.Notify -= NotifyVehicleAdded;
        }
        public void NotifyVehicleRemoved(Vehicle vehicle)
        {
            Console.Clear();
            Console.WriteLine($"{vehicle.ToString()} have been removed");
            _parkingController.Notify -=NotifyVehicleRemoved;
        }
        public void NotifyVehicleBalanceToppedUp(Vehicle vehicle)
        {
            Console.Clear();
            Console.WriteLine($"{vehicle.ToString()} have been topped up");
            _parkingController.Notify -= NotifyVehicleBalanceToppedUp;
        }
    }
}