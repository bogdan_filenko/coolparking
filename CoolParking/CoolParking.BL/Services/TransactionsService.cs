using System.Collections.Generic;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Text;
using System.Linq;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TransactionsService : ITransactionsService
    {
        private readonly Parking _parking;
        private readonly ILogService _logService;
        private List<TransactionInfo> _parkingTransactions;
        private readonly ITimerService _logTimer;
        public TransactionsService(ILogService logService, ITimerService logTimer)
        {
            _parking = Parking.GetInstance();
            _logService = logService;
            
            _logTimer = logTimer;
            _logTimer.Elapsed += _LogLastParkingTransactions;
            _logTimer.Start();

            _parkingTransactions = new List<TransactionInfo>();
        }
        private void _LogLastParkingTransactions(object sender, ElapsedEventArgs e)
        {
            StringBuilder logBuilder = new StringBuilder();
            foreach (var transactionInfo in _parkingTransactions)
            {
                logBuilder.Append(transactionInfo.ToString() + "\n");
            }
            _logService.Write(logBuilder.ToString());
            _parkingTransactions.Clear();
        }
        public void AddTransactionInfo(TransactionInfo transactionInfo)
        {
            _parkingTransactions.Add(transactionInfo);
        }
        public TransactionInfo[] GetLastTransactions()
        {
            return _parkingTransactions.ToArray();
        }
        public string GetAllTransactionsHistory()
        {
            return _logService.Read();
        }
        public Vehicle TopUpVehicle(string vehicleId, decimal sum)
        {
            if (!VehicleValidator.ValidateVehicleId(vehicleId))
            {
                throw new ArgumentException("Invalid format of vehicle id");
            }
            if (sum < 0)
            {
                throw new ArgumentException("The sum cannot be negative");
            }
            Vehicle vehicle = _parking.ParkedVehicles.FirstOrDefault(vehicle => vehicle.Id == vehicleId);
            if (vehicle == default)
            {
                throw new InvalidOperationException($"The vehicle with id {vehicleId} is not found");
            }
            vehicle.Balance += sum;
            return vehicle;
        }
    }
}