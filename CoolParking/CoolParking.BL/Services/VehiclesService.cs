using CoolParking.BL.Interfaces;
using System.Collections.ObjectModel;
using CoolParking.BL.Models;
using System.Linq;
using System;
using System.Timers;
using static CoolParking.BL.Models.Settings;

namespace CoolParking.BL.Services
{
    public class VehiclesService : IVehiclesService
    {
        private readonly Parking _parking;
        private readonly ITransactionsService _transactionsService;
        private readonly ITimerService _withdrawService;
        public VehiclesService(ITimerService withdrawService, ITransactionsService transactionService)
        {
            _parking = Parking.GetInstance();
            
            _withdrawService = withdrawService;
            _withdrawService.Elapsed += _WithdrawVehiclesBalances;
            _withdrawService.Start();

            _transactionsService = transactionService;
        }
        private void _WithdrawVehiclesBalances(object sender, ElapsedEventArgs e)
        {
            foreach (var vehicle in _parking.ParkedVehicles)
            {
                decimal tariff = tariffs[vehicle.VehicleType];
                decimal sum = 0;
                if (vehicle.Balance < tariff && vehicle.Balance <= 0)
                {
                    sum = tariff * FineRation;
                }
                else if (vehicle.Balance < tariff && vehicle.Balance > 0)
                {
                    sum = vehicle.Balance + (tariff - vehicle.Balance) * FineRation;
                }
                else
                {
                    sum = tariff;
                }
                _parking.Balance += sum;
                vehicle.Balance -= sum;
                _transactionsService.AddTransactionInfo(new TransactionInfo(sum, DateTime.Now, vehicle.Id));
            }
        }
        public ReadOnlyCollection<Vehicle> GetVehiclesList()
        {
            return _parking.ParkedVehicles.AsReadOnly();
        }
        public Vehicle GetVehicle(string vehicleId)
        {
            if (!VehicleValidator.ValidateVehicleId(vehicleId))
            {
                throw new ArgumentException("Invalid format of vehicle id");
            }
            Vehicle vehicle = _parking.ParkedVehicles.FirstOrDefault(v => v.Id == vehicleId);
            if (vehicle == default)
            {
                throw new InvalidOperationException($"The vehicle with id {vehicleId} is not found");
            }
            return vehicle;
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.ParkedVehicles.Find(v => v.Id == vehicle.Id) != null)
            {
                throw new ArgumentException("The vehicle with the same id is already exist");
            }
            if (_parking.ParkedVehicles.Count == ParkingCapacity)
            {
                throw new InvalidOperationException("No free places at the parking");
            }
            _parking.ParkedVehicles.Add(vehicle);
        }
        public void RemoveVehicle(string vehicleId)
        {
            if (!VehicleValidator.ValidateVehicleId(vehicleId))
            {
                throw new ArgumentException("Invalid format of vehicle id");
            }
            Vehicle vehicle = _parking.ParkedVehicles.FirstOrDefault(vehicle => vehicle.Id == vehicleId);
            if (vehicle == null)
            {
                throw new InvalidOperationException($"The vehicle with id {vehicleId} is not found");
            }
            if (vehicle.Balance < 0)
            {
                throw new ArgumentException("The vehicle has unpaid bills");
            }
            _parking.ParkedVehicles.Remove(vehicle);
        }
    }
}