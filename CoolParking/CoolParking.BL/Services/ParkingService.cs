﻿using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking _parking;

        public ParkingService()
        {
            _parking = Parking.GetInstance();
        }
        public decimal GetBalance()
        {
            return _parking.Balance;
        }
        public int GetCapacity()
        {
            return _parking.Capacity;
        }
        public int GetFreePlaces()
        {
            return _parking.Capacity - _parking.ParkedVehicles.Count;
        }
    }
}