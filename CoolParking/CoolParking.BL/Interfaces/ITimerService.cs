﻿using System.Timers;
using System;

namespace CoolParking.BL.Interfaces
{
    public interface ITimerService : IDisposable
    {
        event ElapsedEventHandler Elapsed;
        double Interval { get; set; }
        void Start();
        void Stop();
    }
}
