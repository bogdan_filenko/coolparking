using System.Collections.ObjectModel;
using CoolParking.BL.Models;

namespace CoolParking.BL.Interfaces
{
    public interface IVehiclesService
    {
        ReadOnlyCollection<Vehicle> GetVehiclesList();
        Vehicle GetVehicle(string vehicleId);
        void AddVehicle(Vehicle vehicle);
        void RemoveVehicle(string vehicleId);
    }
}