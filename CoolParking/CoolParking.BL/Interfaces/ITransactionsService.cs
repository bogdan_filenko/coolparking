using CoolParking.BL.Models;

namespace CoolParking.BL.Interfaces
{
    public interface ITransactionsService
    {
        TransactionInfo[] GetLastTransactions();
        string GetAllTransactionsHistory();
        Vehicle TopUpVehicle(string vehicleId, decimal sum);
        void AddTransactionInfo(TransactionInfo transactionInfo);
    }
}