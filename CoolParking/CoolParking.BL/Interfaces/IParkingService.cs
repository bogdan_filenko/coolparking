﻿
namespace CoolParking.BL.Interfaces
{
    public interface IParkingService
    {
        decimal GetBalance();
        int GetCapacity();
        int GetFreePlaces();
    }
}
