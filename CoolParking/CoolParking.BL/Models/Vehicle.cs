﻿using System;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using static CoolParking.BL.Models.Settings;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        [JsonPropertyName("id")]
        public string Id { get; init; }
        [JsonPropertyName("vehicleType")]
        public VehicleType VehicleType { get; init; }
        [JsonPropertyName("balance")]
        public decimal Balance { get; set; }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (!VehicleValidator.ValidateVehicleId(id))
            {
                throw new ArgumentException("Id argument is invalid");
            }
            this.Id = id;
            this.VehicleType = vehicleType;
            this.Balance = balance;
        }
        public override string ToString()
        {
            string vehicleType = Enum.GetName<VehicleType>(this.VehicleType);
            return $"{vehicleType}: {this.Id} --- {this.Balance}";
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            StringBuilder vehicleIdBuilder = new StringBuilder();
            Random random = new Random();
            foreach (var letter in VehicleIdTemplate)
            {
                if (letter == 'X')
                {
                    vehicleIdBuilder.Append(Convert.ToChar(65 + random.Next(0, 25)));
                }
                else if (letter == 'Y')
                {
                    vehicleIdBuilder.Append(random.Next(0, 9));
                }
                else
                {
                    vehicleIdBuilder.Append(letter);
                }
            }
            return vehicleIdBuilder.ToString();
        }
    }
    public class VehicleValidator
    {
        public static bool ValidateVehicleId(string vehicleId)
        {
            Regex regex = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$");
            if (regex.IsMatch(vehicleId))
            {
                return true;
            }
            return false;
        }
    }
}