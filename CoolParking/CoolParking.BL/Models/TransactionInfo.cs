﻿using System;
using System.Text.Json.Serialization;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        [JsonPropertyName("vehicleId")]
        public string VehicleId { get; set; }
        [JsonPropertyName("sum")]
        public decimal Sum { get; set; }
        [JsonPropertyName("date")]
        public DateTime Date { get; set; }
        public TransactionInfo(decimal sum, DateTime date, string vehicleId)
        {
            Sum = sum;
            Date = date;
            VehicleId = vehicleId;
        }
        public override string ToString()
        {
            return $"{Date.ToString()}: {Sum} money withdrawn from vehicle with Id=\'{VehicleId}\'.";
        }
    }
}
