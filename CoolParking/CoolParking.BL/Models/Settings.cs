﻿using System.Collections.ObjectModel;
using System.Collections.Generic;
using System;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const int ParkingInitialBalance = 0;
        public static ReadOnlyDictionary<VehicleType, decimal> tariffs = new ReadOnlyDictionary<VehicleType, decimal>(
            new Dictionary<VehicleType, decimal>(new KeyValuePair<VehicleType, decimal>[] 
            {
                new KeyValuePair<VehicleType, decimal>(VehicleType.PassengerCar, 2),
                new KeyValuePair<VehicleType, decimal>(VehicleType.Truck, 5),
                new KeyValuePair<VehicleType, decimal>(VehicleType.Bus, 3.5m),
                new KeyValuePair<VehicleType, decimal>(VehicleType.Motorcycle, 1)
            })
        );
        public const int ParkingCapacity = 10;
        public const int WriteOffPeriod = 5;
        public const int LogWritePeriod = 60;
        public const decimal FineRation = 2.5m;
        public const string VehicleIdTemplate = "XX-YYYY-XX";
    }
}